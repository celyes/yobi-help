---
tags:
 - dnd
 - turn
 - off
 - quiet
---
# How to activate do not disturb?

Yobi offers you a do not disturb mode which when activated, suppresses all incoming calls and message. You'll still be able to receive messages and calls just as normal but ringing will be disabled besides that notifications won't be shown.

How to activate do not disturb ?

Click on your avatar. You'll see a side menu that contains a switch besides the sentence "Do not disturb". Toggle this switch to activate/deactivate it.

<hr />

<feedback />
