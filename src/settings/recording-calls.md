---
tags:
 - register
 - record
---
# Recording Calls
Call recording is turned on by default for your Yobi account.

To turn it off when logged into your browser or desktop:

1. Select Settings from the left menu

2. Go to Channels and click on edit to toggle ON/OFF the call recording

Watch here:

<explanation link="https://www.youtube.com/embed/XrYjemMsrVs" />

To turn call recording on/off when logged into mobile:

1. Select Settings from the left menu

2. Go to Channels and click on edit to toggle ON/OFF the call recording

Watch here:

<explanation link="https://www.youtube.com/embed/9_ENZHYHAx4" />

<hr />

<feedback />