---
tags:
 - add
 - invite
 - send invitation
 - manage users
---
# Adding Users to Your Yobi Account
Adding your team to your Yobi is recommended so you can get conversations going with your customers.
1. To add your team, head over to the Settings page located at the bottom of the left sidebar.
2. Click on Manage Users
3. Click on Add User and type in their information
4. Send an invite through email or text

<explanation link="https://www.youtube.com/embed/zfhLXripIcY" />

<hr />

<feedback />