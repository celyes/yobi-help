---
tags:
 - inbox
 - old
---
# Archive Messages

<explanation link="https://www.youtube.com/embed/GH-wnpN3Xkk" />

Organize your inbox and archive old messages.

Watch video above on how simple archiving is with Yobi.

<hr />
<feedback />