# Settings

Know more about settings of Yobi apps. You can browse the list of links below to move to a specific location or just use the search bar on the top for quick navigation.

- [Add users to your team][0]
- [Archive your conversations][1]
- [Updating your accout picture][2]
- [Update a contact's picture][2]
- [Changing and specifying business hours][3]
- [Add custom greeting message to your account][4]
- [Configure call forwarding][5]
- [configure call transfer][6]
- [Add custom voicemail audio][7]
- [Setting **do not disturb**][8]
- [Configure call recording options][9]

<hr />

<feedback />

<!-- adding  users -->
[0]: /settings/adding-users.html
<!-- archive messages -->
[1]: /settings/archive-messages.html
<!-- avatars -->
[2]: /settings/avatar.html
<!-- business hours -->
[3]: /settings/business-hours.html
<!-- custom greeting -->
[4]: /settings/custom-greeting.html
<!-- call forwarding -->
[5]: /settings/call-forwarding.html
<!-- call transfer -->
[6]: /settings/call-transfer.html
<!-- Custom voicemail -->
[7]: /settings/custom-voicemail.html
<!-- Do not disturb -->
[8]: /settings/do-not-disturb.html
<!-- recording calls -->
[9]: /settings/recording-calls.html