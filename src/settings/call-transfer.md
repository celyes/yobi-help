---
tags:
 - switch
 - redirect
 - audio
---
# Call Transfers
Transfer Yobi incoming calls to anyone from your team.

1. Make sure you've added your team as User/Admin to your account. Here's how: 

2. While on an inbound call, click on the Transfer button to choose the team member you would like to transfer your call to.

3. Once they're connected, you can now end your call and they will continue their conversation from there.

Watch how it works:

<explanation link="https://www.youtube.com/embed/1MKICGeqGFg" />

<hr />

<feedback />