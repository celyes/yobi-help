---
tags:
 - voice
 - record
 - custom
---
# Adding a Custom Greeting Message

<explanation link="https://www.youtube.com/embed/XncOUykGX4M" />

Adding a custom greeting in Yobi is easy!

1. Log in to your Yobi
2. Go to Settings then Channels and click the Greeting icon
3. Upload your pre-recorded custom greeting in .mp3 or .wav format

That's it!

Enjoy seamless conversations in one app, Yobi.

Don't have an account? Sign up for a free trial here

<hr />

<feedback />