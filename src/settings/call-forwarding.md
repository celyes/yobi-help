---
tags:
 - audio
 - forward
 - redirect
---
# How to activate call forwarding

Call forwarding allows you to redirect incoming calls to a number that you specify.

To activate call forwarding, follow these steps:

Click on your avatar → channels → my channels → the phone number you wanna forward from → edit.

You'll move to a view that allows you to edit channel information. click on "Add number" beside "Forward calls" and type in the number you want to forward to.

<explanation link="https://player.vimeo.com/video/689406908" />
<hr />
<feedback />