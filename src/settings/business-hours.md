---
tags:
 - activity
 - hours
 - time
 - span
---
# How to activate and change business hours?

Business hours is one of Yobi's most useful features. It allows you to receive calls and messages only in a defined time span that corresponds to the hours that your business usually works in. You can also customize the timezone for even more precision in this regard.

To activate business hours, follow these steps:

Click on your avatar → Channels → My channels → Click on the specified number → Business hours

You'll get to this screen:

<img src="/images/business-hours-mobile.jpg" height="600" />

Clicking on a specific day will allow you change the hours for that day. You can also select all day by checking or unchecking the box beside each day.

For web and desktop applications, Follow this video:

<explanation link="https://player.vimeo.com/video/689410333" />

<hr />

<feedback />