---
tags:
 - personal
 - picture
 - avatar
---
# Add a Photo to Your Profile and Contacts

<explanation link="https://www.youtube.com/embed/lItCozOy630" />

Personalize your Yobi by adding a photo to your Profile and Contacts!

1. Go to Settings
2. Choose Profile and click the upload button to upload your profile photo
3. Go to Contacts
4. Choose a contact and click on the photo beside the contact name 
5. Upload a photo for your contact

<hr />

<feedback />