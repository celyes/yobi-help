---
tags:
 - voice
 - record
 - custom
 - mail
---
# Setup a Custom Voicemail Message

<explanation link="https://www.youtube.com/embed/fLoF4ovZUBo" />

Here's how to add a custom voicemail message:

1. Log in to your Yobi
2. Go to Settings then Channels and click the Voicemail icon
3. Upload your pre-recorded custom greeting in .mp3 or .wav format

That's it!

<hr />

<feedback />