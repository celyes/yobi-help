---
tags:
  - download
  - link
  - playstore
  - appstore
---
# Download the Yobi App
## Download Yobi for Windows
The Yobi app for Windows is here! 

Now you can get real time notifications from your contacts from your desktop.

How to Install Yobi for Windows:

1. Click here to [download][windows link] the Yobi desktop app for Windows

2. The download should start automatically. Once finished, the Yobi window will appear and you can enter your login to start using Yobi!

## Use the Yobi Web App
The Yobi web app allows you to access your Yobi account without having to download a specific Windows, Mac or phone app.

To get the web app just download the app [here][web app].

Once downloaded, just login or create an account and you're good to go.

If you have any questions regarding downloading Yobi, please check out this website.

## Download Yobi for Mac
If you use a Mac and would like to download Yobi for your device, use this [link][mac app] to get Yobi.

Once downloaded, just login or sign up following the prompts. For further questions, please review our other articles.

## Download Yobi for Android
If you would like to bring Yobi on the go, then you can download the app for your device from [Google Play Store][yobi android app]

Sign in like normal to access your businesses account on Yobi or create one on our website.

## Download Yobi for iOS
If you would like to take Yobi on the go, you can now do so with our iOS app for Apple users! Check out our app on the [Apple App Store][yobi ios app].

Just sign in like normal for your Yobi business account or sign up on our website!

<hr />

<feedback />

[windows link]: https://f.hubspotusercontent00.net/hubfs/8856302/Yobi%20Setup.exe
[web app]: https://app.yib.io
[mac app]: https://f.hubspotusercontent00.net/hubfs/8856302/Yobi%20for%20Mac.dmg
[yobi android app]: https://play.google.com/store/apps/details?id=com.yobi.mobile&hl=en_US&gl=US
[yobi ios app]: https://apps.apple.com/us/app/yobi-connect/id1545096134