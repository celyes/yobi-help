---
tags:
 - free
 - starter
 - pro
 - enterprise
 - offers
 - compare
 - comparison
---
# Explore Product and Plans
## Yobi Plans and Features
Yobi will always have a free tier for users. However, if you and your business are interested in the features that come with the paid tiers, there are 3 paid subscription plans to choose from.
Plans
- Free

Gives you a free Yobi number, lets you connect your business Facebook Messenger to receive and respond to messages
- Starter - $10 a month per user for the monthly plan or $7.47 a month per user for the annual plan

Unlimited Facebook messaging, unlimited texting, and unlimited calls
- Pro - $25 a month per user for the monthly plan or $19.84 a month per user for the annual plan

Everything from the Starter plan plus unlimited call recordings and transcripts, advanced team management, and call tracking
- Enterprise - depending on what your business needs are, pricing can be discussed with our Sales team

Everything fro the Pro plan plus access to premium integrations like Hubspot, Salesforce, and Shopify

If you have any other questions related to pricing for Yobi, please check out our [pricing page on our website][0].

## Benefits of Yobi on a Paid Plan
Yobi offers a free trial so you can experience all the benefits of Yobi with full access. However, after that trial is up, you go back to the basic plan that only offers a free phone number and Facebook messaging. Being able to call and keep those calls recorded helps your business in so many ways just as having text messages helps greatly as well.

Communicating with your clients and making sure they can get in touch with you in an easy and immediate way is vital to a business and its success. By using Yobi, you can always keep up with your customers and keep them happy by having a long-standing positive relationship with that customer.

For more information on how Yobi can help your business, please contact us or check out our website for more information.

## Message Limits on a Free Version of Yobi
Yobi starts as a free 45 day trial, in this trial, you have access to all of Yobi's features. However, after that trial has ended, you can either stick to the free version of Yobi or pay for one of our other options.

If you do decide to keep to the free version, your business will be able to access up to 10k Facebook messages. Once that limit is reached, past messages will start to become unavailable to make up for the new messages coming in.

The free version that Yobi offers does not come with text messaging but you do have access to website chat.

<hr />

<feedback /> 

[0]: https://yobi.app/pricing/

