---
tags:
 - introduction
 - start
 - how to
 - howto
 - conversations
---
# Introduction to Yobi
## What is Yobi?
Yobi is a conversations app you can use as a second phone number for your business. With Yobi, you can have all your customer conversations in one app.

Yobi unifies all your customer conversations and puts them into an intuitive mobile-first interface where you or your team can make a call, respond to a text message, Facebook Messenger, or Twitter direct messages. 

When connected to your Calendly or Zoom account, you can create and share meeting links to your contacts in one app.

If you have an existing Hubspot or Shopify account, with Yobi you can follow up with leads and customers without having to login to different websites. 

Yobi makes it easier for you to be an everyday rockstar.

## Tour The Yobi App
Once you create an account with Yobi, you will be able to go through the app fully to get an idea of how Yobi works for you and your business.

<explanation link="https://player.vimeo.com/video/495824179" />

There are a couple of main sections you'll want to get to know. These include:

1. The top bar
2. The sidebar
3. And the settings menu
- The top bar includes your dialer in which you can make, receive and manage calls. A quick-access message button to bring you directly to messages. The help icon in which you can access yobi.help and get step-by-step walkthroughs of how to use Yobi. Finally, the top bar allows you to access your account and log out.
- The sidebar includes your messages panel, history for all past communications, contacts, and settings.
- The settings menu is located within the sidebar and gives you access to a lot of important material. Within the settings menu, there are 5 subsections. These include:
1. Your Profile - this is where you can verify your email, change your password and see your access type.
2. Manage Users - here you can add users, send users invites, or delete users.
3. Channels - this is where you can connect your business social media, buy a phone number, and see which channels are already linked.
4. Integrations - this is where you can add, manage, or delete integrations like Shopify, Zoom, and Calendly.
5. Import Contacts - this is where you can import contacts from a Gmail, Outlook, or a csv file


## Yobi App Requirements
Yobi is a multichannel conversations app that is best suited for heavy customer conversation-focused businesses. Yobi is software only and therefore can be accessed through the web, download for desktop, tablet, or mobile without the need to buy extra hardware.

For any other questions regarding Yobi, please check out our other help articles.

<hr />

<feedback /> 