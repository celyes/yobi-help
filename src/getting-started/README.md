# Getting started with Yobi

[Introduction to Yobi][0]
<br>
What Yobi is all about
<hr>

[Downloading Yobi apps][1]
<br>
Getting Yobi to work on your phone will take no more than few seconds.
<hr>

[Creating an account][2]
<br>
Create an account and start right away.
<hr>

[Explore products and plans][3]
<br>
Pick the most efficient plan for your business.

<br>
<feedback />

[0]: /getting-started/introduction.html
[1]: /getting-started/downloading-yobi.html
[2]: /getting-started/joining-yobi.html
[3]: /getting-started/plans.html
