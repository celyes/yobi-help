---
tags:
 - open
 - join
 - signup
 - register
 - sign up
 - start
---

# Open or Join a Yobi Account
## Signup for Yobi
To signup for Yobi, simply go to our [Signup Page][0]

Once successful, you will be guided into choosing your Yobi phone number and adding your team as users

<explanation link="https://player.vimeo.com/video/495291362" />

## Introducing Yobi to Your Team
Just like any new software, Yobi can take a bit of time to learn. Good news is we provide many tutorials and short videos to walk you through everything! Introducing your team to Yobi shouldn't take too long at all. Just follow the videos and walkthroughs and you should be good to go.

There are 2 different types of users on Yobi, admins and users. Admins can add or remove team members on Yobi through the Manage Users portal, install, connect or disconnect channels and integrations and import contacts. Users can access Yobi but cannot access the settings page to do the above mentioned actions.

We recommend trying to walk your team through Yobi together that way everyone can learn off of one another and each other's questions. If you or your team need a more in-depth walkthrough of Yobi, please contact us and we can set up a demo with one of our customer support representatives.

## Join a Team
If a business already has an account with Yobi and has new team members join their team, adding these team members to Yobi is easy.

To add a team member:
1. Have someone who has admin access on your team go to Settings in the left hand side bar.
2. Go to Manage Users and click Add User
3. Add in their email, name, phone number and a temporary password
4. Decide if they will have admin access or user access and click the corresponding option.
5. From there save your changes and send them an invite over email by going to their name and hitting actions-send invitation-email.

<hr />

<feedback /> 

[0]: https://app.yib.io/signup

