---
home: false
search: false
pageClass: main-page
# heroImage: https://app.yib.io/assets/images/Logo.svg
# tagline: How can we help you?
# actionText: Get help →
# actionLink: /getting-started/
# features:
# - title: Discover
#   details: Learn about the amazing things Yobi can do for you
# - title: Get help
#   details: Get help and excel at using your Yobi account
# - title: Achieve more
#   details: Achive the most out of Yobi's platforms, integrations and ecosystem
footer: Copyright © 2022, All Rights Reserved
---
<div style="text-align: center">
  <img src="https://app.yib.io/assets/images/Logo.svg" />
  <h1> How can we help you? </h1>
  <SearchBox :isInMain="true" />
  <Features />
  <Footer />
</div>


