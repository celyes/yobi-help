const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Help center',
  base: '/yobi-help/',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#8461c9' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'icon', href: '/images/yobi-favicon.png' }]
  ],
  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    logo: '/images/yobi-logo.png',
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    // lastUpdated: true,
    nav: [
      {
        text: 'Getting started',
        link: '/getting-started/',
      },
      { text: 'Phone numbers', link: '/phone-numbers/' },
      {
        text: 'Additional resources',
        ariaLabel: 'Additional resources',
        items: [
          { text: 'Channels and Integrations', link: '/integrations/' },
          { text: 'Settings', link: '/settings/' }
        ]
      },
      {
        text: 'Contact us',
        link: '/ticket/',
      },
      {
        text: 'Yobi.app',
        link: 'https://yobi.app'
      }
    ],
    sidebar: {
      '/getting-started/': [
        {
          title: 'Getting started',
          collapsable: false,
          children: [
            '',
            'welcome',
            'introduction',
            'downloading-yobi',
            'joining-yobi',
            'plans'
          ]
        }
      ],
      '/phone-numbers/': [
        {
          title: 'Phone numbers',
          collapsable: false,
          children: [
            '',
            'buying-number',
            'porting-a-number',
            'porting-faq',
          ]
        }
      ],
      '/integrations/': [
        {
          title: 'Integrations',
          collapsable: false,
          children: [
            '',
            'twitter',
            'facebook',
            'web-chat',
            'zoom',
            'calendly'
          ]
        }
      ],
      '/settings/': [
        {
          title: 'Settings',
          collapsable: false,
          children: [
            '',
            'adding-users',
            'custom-greeting',
            'avatar',
            'archive-messages',
            'recording-calls',
            'call-transfer',
            'custom-voicemail',
            'do-not-disturb',
            'business-hours',
            'call-forwarding'
          ]
        }
      ],
      '/ticket/': [
        {
          title: 'Submit a ticket',
          collapsable: false,
          children: ['']
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
