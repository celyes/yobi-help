# Phone Numbers
## How do I Get a Yobi Phone Number?

After sign up, here's how to get your free Yobi phone number:

1. Download Yobi to your mobile:

[Android][Yobi android app]

[iOS][Yobi iOS app]

2. Open the Yobi app on your mobile and go to Menu > Channels

3. Click on Phone - Add phone numbers

4. Search for the area code of your preferred number

5. Once the results show, choose the best phone number for you and click on the Add button beside it

That's it! Now you have your dedicated Yobi business phone for texting and calling customers, friends, and more.

<hr />

<feedback />

[Yobi android app]: https://yobi.app/download/android/
[Yobi iOS app]: https://yobi.app/download/ios/