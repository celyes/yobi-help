---
tags:
 - old
 - port
 - carrier
 - transfer
---
# Porting numbers

## Porting a Local Number
Paid subscription plans has porting as one of the features. To put in a porting request:

1. Register an account at [https://yobi.help/help-center](https://yobi.help/help-center)
2. Open a ticket requesting to port in your local number and attach your latest bill from your current provider.
3. Wait for confirmation from a support agent when your phone number has been successfully ported into Yobi.
4. Start calling and sending SMS from Yobi using your own local number.

## Porting a Toll-free Number
Porting a toll-free number is a bit more complex than porting a landline number. With toll-free numbers, there are 2 main steps.
1. The number you want to port needs to be "released" from the old carrier to the new one. Then the new carrier finishes the final process of re-routing the number.
3. Once the number is released, it's standard practice that the old carrier continues service on the number until the new carrier can finish the process of porting the number.

The old carrier, whether that be Yobi because you want to port the phone number out, or the carrier you had the number with before wanting to switch it over to Yobi, will have to work with the new carrier to port the number. Until the 2 carriers can get in contact to begin the process, the porting will not occur.

## Can I Port Out my Yobi Number?
Yes you can! However, it does require a bit of work on your end. For starters, in order to port your number out of Yobi, you will need a carrier who will then be in charge of your number. Once you decide on a carrier, you must then get in contact with them to let them know you want to move your number over to them.

Once a carrier is made aware of you wanting the change, the 2 carriers, the one you chose and us must get in contact to port the number. Porting numbers isn't instant and can take some time. While you are waiting for the number to be ported, Yobi will continue to support the number and you, until the porting is complete.

<hr />

<feedback /> 