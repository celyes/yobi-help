---
tags:
 - old
 - port
 - carrier
 - transfer
---
# Porting FAQ's
Porting your US telephone number is a process that can take up to four (4) weeks depending on the type of number you are porting and the country the number is located in. The process includes coordination between our provider and your service provider to transfer the number to Yobi

**What is porting?**

Number porting is the process of taking an existing phone number and transferring it to another provider, in this case, taking an existing phone number and making it your main number for Yobi.

**What kind of phone numbers can I port?**

Great question! You can port local numbers, numbers from all around the United States, and toll free numbers!

**Can I port my business phone number into Yobi?**

Yes you can! You will need to send us an email at [yobi@yobi.app](mailto:yobi@yobi.app) letting us know you want to port in your number/s and we'll get back to you asap with the next steps.

<hr />

<feedback /> 