

# All about phone numbers

[Buying phone number][0]
<br>
Buy a phone number and start your journey with Yobi.
<hr>

[Porting a number to Yobi][1]
<br>
Send and receive Messenged messages via Yobi integration for Messenger.
<hr>

[Questions about porting your number][2]
<br>
Send and receive Facebook business page messages via Yobi integration for Facebook.

<br>
<feedback />

[0]: /phone-numbers/buying-a-number.html
[1]: /phone-numbers/porting-a-number.html
[2]: /phone-numbers/porting-faq.html
