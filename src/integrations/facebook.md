---
tags:
 - meta
 - dm
 - business
---
# How to Connect your Facebook Page
With Yobi you can send and respond to Facebook Messenger messages in one app.
<explanation link="https://player.vimeo.com/video/508914769"></explanation>

1. Go to Settings in the Left Sidebar.
2. Click the button, "Channels"
3. Select the Facebook option and use the login for the page you want to connect to your account. 

Congratulations! Now you can send and receive Facebook Messenger messages from Yobi.

<hr />

<feedback />