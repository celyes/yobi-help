---
tags:
 - video
 - call
 - videocall
 - meeting
---
# Connecting Your Zoom to Yobi

<explanation link="https://www.youtube.com/embed/QcIG3Ag34RU" />

With Yobi you can start Zoom Meetings with your contacts in one app.

1. Go to Settings > Integrations and click Zoom
2. A new screen will ask you to authorize Yobi to connect to your Zoom account
3. Choose a contact you want to start a Zoom meeting with
4. Click the Zoom icon beside the Call icon
5. Copy the Zoom Meeting link and send as a message to a contact

Now you can start your Zoom meeting from one app!

<hr />

<feedback />