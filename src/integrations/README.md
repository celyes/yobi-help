

# Integrations

[Twitter][0]
<br>
Send and receive Twitter direct messages via Yobi integration for Twitter.
<hr>

[Facebook][1]
<br>
Send and receive Facebook business page messages via Yobi integration for Facebook.
<hr>

[Messenger][2]
<br>
Send and receive Messenged messages via Yobi integration for Messenger.
<hr>

[Calendly][3]
<br>
Increase productivity and save time by using Yobi integration for Calendly. 
<hr>

[Zoom][4]
<br>
Schedule Zoom meetings using Yobi integration for Zoom. 

<br>
<feedback />

[0]: /integrations/twitter.html
[1]: /integrations/facebook.html
[2]: /integrations/messenger.html
[3]: /integrations/calendly.html
[4]: /integrations/zoom.html
