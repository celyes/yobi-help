---
tags:
 - messenger
 - dm
 - direct messages
---
# How to connect your messenger account to Yobi

<explanation link="https://www.youtube.com/embed/rk3t-MAjwVE" />

Install the Facebook Chat plugin to your website and start receiving and responding to your business Facebook Messenger DM's from Yobi.

1. Set up the Chat Plugin from your Facebook Page. You can follow the instructions according to Facebook's guidelines here

2. Connect your business Facebook page to your Yobi account. Follow the instructions here 

<hr />

<feedback />