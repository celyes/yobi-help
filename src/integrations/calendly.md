---
tags:
 - appointment
 - calendar
 - meeting
---
# Yobi integration for Calendly

<explanation link="https://www.youtube.com/embed/4XDFVbSYmTw" />

Share your Calendly meetings link with your Yobi contacts.

1. Go to Settings >Integrations and click Calendly. 
Once connected, the window will reload and you will see that the Calendly integration is marked as “linked”

2. Copy and share your Calendly meeting link to your contacts from Yobi!

<hr />

<feedback />
