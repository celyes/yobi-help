---
tags:
 - dm
 - tweet
---
# Connecting Twitter to Yobi

With Yobi you can send and respond to Twitter messages in one app.
Once you connect your Twitter account, conversations can be accessed by your entire team so you don’t miss a direct message ever again.

<explanation link="https://www.youtube.com/embed/PzETDhUeCH8" />

1. Go to Settings > Channels and click Twitter integration
2. A window will pop up to start the Twitter integration
3. Click “Authorize app” to connect your Twitter account to your Yobi account
4. Now you can receive and reply to Twitter messages on Yobi!

<hr />

<feedback />